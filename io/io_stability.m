function io_stability(label, util, stab, min_rank, max_rank, ave_rank, var_rank, ...
    min_util, max_util, ave_util, var_util, filepath, K)

file = fopen(filepath, 'w');

for cls=1:numel(util)
    
    fprintf(file ,'Class = %d\n', cls); 
    
    U = util{cls}(:,2);
    U = U / max(U);
    
    fprintf(file, 'Rank:\n');
    for i=1:min(size(util{cls},1),K)
        id = util{cls}(i,1);
        fprintf(file, '%d\t%s\t%.4f\t%.4f\t%.4f\t%.4f\n',...
             i, label{id}, min_rank{cls}(i), max_rank{cls}(i), ...
            ave_rank{cls}(i), var_rank{cls}(i));
    end
    fprintf(file, '\n\n');
    
    fprintf(file, 'Utility:\n');
    for i=1:min(size(util{cls},1),K)
        id = util{cls}(i,1);
        fprintf(file, '%s\t%.4f\t%.4f\t%.4f\t%.4f\t%.4f\n',...
            label{id}, U(i), min_util{cls}(i), max_util{cls}(i), ...
            ave_util{cls}(i), var_util{cls}(i));
    end
    fprintf(file, '\n\n');
    
    fprintf(file, 'Stability:\n');
    for i=1:min(size(util{cls},1), K)
        id = util{cls}(i,1);
        fprintf(file, '%s\t%.4f\t%.4f\n', label{id}, U(i), ...
            stab{cls}(i));
    end
    
end

fclose(file);

end