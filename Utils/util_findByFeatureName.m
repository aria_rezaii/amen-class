function res = util_findByFeatureName(F_label, str)

res = {};
for i=1:numel(F_label)
    if (numel(findstr(lower(F_label{i}), lower(str))) ~= 0)
        res = [res; F_label{i}];
    end
end

end