function [ A, F, A_label, F_label ] = util_refineData( A, F, A_label, F_label )

% Remove singletons
cnt = 1;
fprintf('Before\n');
disp(size(A));
disp(size(F));
while (1)
    fprintf('Removing singletons iteration #%d\n',cnt);
    D = sum(A);
    goodNodes = D > 1;
    
    if (sum(goodNodes) == full(size(A,1)))
        break;
    end
    
    [A, F, A_label, F_label] = util_filterNode(A, F, A_label, F_label,...
        goodNodes);
    cnt = cnt + 1;
end

% Remove nodes without any attribute
sumf = sum(F,2);
goodNodes = sumf > 0;
A = A(goodNodes, goodNodes);
A_label = A_label(goodNodes);
F = F(goodNodes,:);

fprintf('After\n');
disp(size(A));
disp(size(F));

end