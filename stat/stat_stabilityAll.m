function [stab, rank, util, ...
    min_rank, max_rank, ave_rank, var_rank, ...
    min_util, max_util, ave_util, var_util] = stat_stabilityAll(Xs, part, feats, new_label )

MAX_POSSIBLE_RANK = numel(new_label) + 1;
k_feats = numel(new_label);     % Total number of features
m_classes = numel(feats);       % Total number of classes

belong = zeros(k_feats,1);      % To what class a feature belongs
trues = zeros(k_feats,1);       % The number of positive tests for a feature
totes = zeros(k_feats,1);       % Total number of tests for a feature

class = cell(m_classes, 1);     % Class{cls}(i,f) = detected class for feature f on experiment i
rank = cell(m_classes, 1);      % Rank{cls}(i,f) = rank for feature f on experiment i
util = cell(m_classes, 1);      % Util{cls}(i,f) = utility for feature f on experiment i

% We initialized the BELONG and TOTES vectors to their values
for cls = 1:m_classes
    belong(part{cls}) = cls;
    totes(part{cls}) = size(Xs{cls},1);
    
    n_coms = size(Xs{cls},1);
    class{cls} = zeros(n_coms, k_feats);
    rank{cls} = ones(n_coms, k_feats) * MAX_POSSIBLE_RANK;
    util{cls} = zeros(n_coms, k_feats);
end

for cls = 1:m_classes
    
    n_coms = size(Xs{cls},1);
    
    for com = 1:n_coms
        newXs = Xs;
        % We remove one community for this "cls"
        newXs{cls} = newXs{cls}(setdiff(1:n_coms, com),:);
        
        % We create a new partitioning using this new set of communities
        [newPart, ~, ~] = main_partition(newXs, feats, new_label);
        [newUtil, ~, ~] = main_filterUtility(newXs, newPart, new_label, feats, .25);
        
        P = newUtil{cls}(:,1);
        R = 1:numel(P);
        R = R / max(R);
        U = newUtil{cls}(:,2);
        U = U / max(U);

        for fIdx = 1:numel(P)
            % The current feature in the new partitioning
            f = P(fIdx);
            if (belong(f) == cls)   % If it originally belonged to this same class
                % We add one to the number of true tests for "f"
                trues(f) = trues(f) + 1;
                rank{cls}(com, f) = R(fIdx);
                if (R(fIdx) == 0)
                    fprintf('RANK FOR %d IN CLASS %d when removing %d is 0.\n', ...
                        fIdx, cls, com);
                    return;
                end
                util{cls}(com, f) = U(fIdx);
            end
        end
    end
    
    % Stability <- N(true)/N(total)
    stab{cls} = trues(part{cls}) ./ totes(part{cls});
    
    % Rank statistics
    max_rank{cls} = max(rank{cls})';
    max_rank{cls} = max_rank{cls}(part{cls});
    min_rank{cls} = min(rank{cls})';
    min_rank{cls} = min_rank{cls}(part{cls});
    ave_rank{cls} = mean(rank{cls})';
    ave_rank{cls} = ave_rank{cls}(part{cls});
    var_rank{cls} = var(rank{cls})';
    var_rank{cls} = var_rank{cls}(part{cls});
    
    % Util statistics
    max_util{cls} = max(util{cls})';
    max_util{cls} = max_util{cls}(part{cls});
    min_util{cls} = min(util{cls})';
    min_util{cls} = min_util{cls}(part{cls});
    ave_util{cls} = mean(util{cls})';
    ave_util{cls} = ave_util{cls}(part{cls});
    var_util{cls} = var(util{cls})';
    var_util{cls} = var_util{cls}(part{cls});
%     
%     figure(cls);
%     boxplot(util{cls}(:,part{cls}(1:10)));
%     title(sprintf('Utility for class %d', cls));
    
end

end