
for party=1:2
    if (party == 1)
        continue;
    end
    for i=1:numel(policy)
        p = stat_policyPercent(utils, util_labels, {policy{i}}, party);
        figure((party-1)*numel(policy) + i);
        stat_congressPlot(p, policy{i}, party);
    end
end