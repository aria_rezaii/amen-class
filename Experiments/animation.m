

load dataset\amazon-Video.mat;
feats = {'Animation' , 'Classics'};
graphname = dataset_name;

% 1. Finding communities
[coms, comHubs, seeds] = main_getComs(A, F, F_label, feats, graphname);

% 2. Filter features
[F,F_label] = util_removeFeatures(F,F_label, {'Under 13', 'Over 13'});
main_F = F;
main_label = F_label;

[F,F_label] = util_removeFeatures(F,F_label,feats);
[F,F_label] = util_filterComFeatures(coms, F, F_label);

% 3. Find AMEN weights
tic;
[Xs, comScores, hubs, new_label] = main_getAmenWeights(A, F, F_label, ...
    feats, coms, comHubs);
time2 = toc;

% 4. Find the partitioning
[part, score, part_label] = main_partition(Xs, feats, new_label);

% 5. Refine the results with Utility
[util, util_label, final_part] = main_filterUtility(Xs, part, new_label, feats, .25);

for cls=1:numel(feats)
    part{cls} = util{cls}(:,1);
end

[stab, R, U, min_rank, max_rank, ave_rank, var_rank, ...
    min_util, max_util, ave_util, var_util] = ...
    stat_stabilityAll(Xs, part, feats, new_label);
% fprintf('Elapsed time without filtering features = %.5f\n', time1);
fprintf('Elapsed time with filtering features = %.5f\n', time2);

% [D,w,y] = lasso_data(F, hubs);
cRange = 2 .^ (-10:10);
F = main_F;
F_label =  main_label;
nodes = {};
for cls = 1:numel(feats)
    fid = util_findFeature(F_label, feats{cls});
    nodes{cls} = find(F(:,fid));
end
[F,F_label] = util_removeFeatures(F,F_label,feats);

K = 10;

[ave_acc, ave_fw, var_fw] = lasso_repeat(F, nodes, cRange, K);
res = lasso_feature(ave_fw, var_fw, F_label);

F = main_F;
F_label = main_label;
freq_res = main_featureFrequency(F, F_label, feats);


