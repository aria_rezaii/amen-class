function [part, score, part_label] = main_partition(Xs, feats, F_label)

m_targets = numel(feats);

part = [];
part_label = [];
score = 0;

for cls=1:m_targets
    sumx = sum(Xs{cls});
    if (max(sumx) == 0)
        return;
    end
end

if (numel(F_label) <= 1000)
    [part, score] = SWP_Amen(Xs);
else
    part = [];
    score = -1;
end
fprintf('Score for SWP algorithm = %.5f\n', score);
[sPart, sScore] = Not_SWP(Xs);
fprintf('Score for simplified algorithm = %.5f\n', sScore);
if (sScore > score)
    part = sPart;
    score = sScore;
end

[dPart, dScore] = SWP_Discrete_Greedy(Xs);
fprintf('Score for Discrete Greedy = %.5f\n', dScore);
if (dScore > score)
    part = dPart;
    score = dScore;
end

part_label = cell(m_targets,1);
for target=1:m_targets
    part_label{target} = F_label(part{target});
end

end